﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject.Modules;
using RideSharingApp.BLL.Services;
using RideSharingApp.BLL.Interfaces;

namespace RideSharingApp.WEB.Util
{
    public class AccountInformationModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IAccountInformationService>().To<AccountInformationService>();
        }
    }
}