﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RideSharingApp.BLL.DTO;
using RideSharingApp.BLL.Interfaces;
using RideSharingApp.WEB.Models;
using AutoMapper;
using RideSharingApp.BLL.Infrastructure;
using System.Net;

namespace RideSharingApp.WEB.Controllers
{
    public class AccountInformationController : Controller
    {
        const string StaticSalt = "1e58c1047ca24fa6b0661dde402cee179860087f1631ab01eeca6a068f3bbb8a";


        IAccountInformationService accountService;
        public AccountInformationController(IAccountInformationService serv)
        {
            accountService = serv;
        }

        [HttpGet]
        public ActionResult Input()
        {
            return View();
        }

        public string GetIPAddress()
        {
            
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];

            return ipAddress.ToString();
        }

        [HttpPost]
        public ActionResult Registration(string Login, string Password, string Email, IPAddress iP, UrlHelper Url)
        {
            try
            {
                var newAccount = new AccountInformationDTO { Login = Login, Password = Password, Email = Email };
                accountService.Registration(newAccount);
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
            }
            return View(Input());
        }

        protected override void Dispose(bool disposing)
        {
            accountService.Dispose();
            base.Dispose(disposing);
        }
    }
}
