﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RideSharingApp.WEB.Models
{
    public class CarViewModel
    {
        public int id { get; set; }
        public string Mark { get; set; }
        public string Model { get; set; }
        public int YearOfIssue { get; set; }
        public string RegistrationNumber { get; set; }
        public string Comments { get; set; }
        public bool Deleted { get; set; }
    }
}