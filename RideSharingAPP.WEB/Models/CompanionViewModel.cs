﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RideSharingApp.WEB.Models
{
    public class CompanionViewModel
    {
        public int ClientId { get; set; }

        public int PassingPointId { get; set; }
    }
}