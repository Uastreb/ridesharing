﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RideSharingApp.WEB.Models
{
    public class AccountInformationViewModel
    {
        public int id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string DynamicSalt { get; set; }
        public string Email { get; set; }
    }
}