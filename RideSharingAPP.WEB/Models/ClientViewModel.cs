﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RideSharingApp.WEB.Models
{
    public class ClientViewModel
    {
        public int id { get; set; }
        public DateTime DateOfRegistration { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Telephone { get; set; }
        public string Comments { get; set; }
        public bool Enabled { get; set; }
        public string Reason_for_blocking { get; set; }
    }
}