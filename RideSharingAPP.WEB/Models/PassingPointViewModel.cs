﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RideSharingApp.WEB.Models
{
    public class PassingPointViewModel
    {
        public int id { get; set; }
        public string OriginCoordinates { get; set; }
        public string EndCoordinates { get; set; }
        public DateTime DateAndTimeOfDepartue { get; set; }
        public DateTime DateAndTimeOfArrival { get; set; }
        public decimal Cost { get; set; }
        public int NumberPointOfThisTrip { get; set; }
        public bool Periodic { get; set; }
    }
}