﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RideSharingApp.WEB.Models
{
    public class DriverLicensesViewModel
    {
        public int id { get; set; }
        public DateTime DateOfReceiving { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string DriverLicensesNumber { get; set; }
        public string IssuingAuthority { get; set; }
        public Enum Categories { get; set; }
    }
}