﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RideSharingApp.WEB.Models
{
    public class TripViewModel
    {
        public int id { get; set; }
        public int NumberOfSeats { get; set; }
        public Enum Status { get; set; }
        public DateTime RegistrationEndDate { get; set; }
        public bool Periodic { get; set; }
    }
}