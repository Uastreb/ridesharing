﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RideSharingApp.DAL.EF;
using RideSharingApp.DAL.Interfaces;
using RideSharingApp.DAL.Entities;

namespace RideSharingApp.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private RideSharingContext db;
        private TRepository<Driver> driverRepository;
        private TRepository<Car> carRepository;
        private TRepository<Client> clientRepository;
        private TRepository<AccountInformation> accountInformationRepository;
        private TRepository<DriversLicense> driverLicenseRepository;
        private TRepository<Trip> tripRepository;
        private TRepository<Companion> companionRepository;
        private TRepository<PassingPoint> passingPointRepository;

        public UnitOfWork(string connectionString)
        {
            db = new RideSharingContext(connectionString);
        }

        public IRepository<Car> Cars
        {
            get
            {
                if (carRepository == null)
                    carRepository = new TRepository<Car>(db);
                return carRepository;
            }
        }

        public IRepository<Driver> Drivers
        {
            get
            {
                if (driverRepository == null)
                    driverRepository = new TRepository<Driver>(db);
                return driverRepository;
            }
        }

        public IRepository<Client> Clients
        {
            get
            {
                if (clientRepository == null)
                    clientRepository = new TRepository<Client>(db);
                return clientRepository;
            }
        }


        public IRepository<DriversLicense> DriversLicenses
        {
            get
            {
                if (driverLicenseRepository == null)
                    driverLicenseRepository = new TRepository<DriversLicense>(db);
                return driverLicenseRepository;
            }
        }

        public IRepository<Trip> Trips
        {
            get
            {
                if (tripRepository == null)
                    tripRepository = new TRepository<Trip>(db);
                return tripRepository;
            }
        }

        public IRepository<PassingPoint> PassingPoints
        {
            get
            {
                if (passingPointRepository == null)
                    passingPointRepository = new TRepository<PassingPoint>(db);
                return passingPointRepository;
            }
        }

        public IRepository<AccountInformation> AccountInformations
        {
            get
            {
                if (accountInformationRepository == null)
                    accountInformationRepository = new TRepository<AccountInformation>(db);
                return accountInformationRepository;
            }
        }


        public IRepository<Companion> Companions
        {
            get
            {
                if (companionRepository == null)
                    companionRepository = new TRepository<Companion>(db);
                return companionRepository;
            }
        }

        public IRepository<AccountInformation> AccountsInformation => throw new NotImplementedException();

        public void Save()
        {
            db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
