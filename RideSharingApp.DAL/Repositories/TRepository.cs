﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using RideSharingApp.DAL.Interfaces;
using RideSharingApp.DAL.EF;
using System.Data.Entity;


namespace RideSharingApp.DAL.Repositories
{
    public class TRepository <TEntity> : IRepository<TEntity> where TEntity : class
    {
        private RideSharingContext context;
        private DbSet<TEntity> db;

      
        public TRepository(RideSharingContext _context)
        {
            this.context = _context;
            db = context.Set<TEntity>();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return db.ToList<TEntity>();
        }

        public TEntity Get(int id)
        {
            return db.Find(id);
        }

        public void Create(TEntity obj)
        {
            context.Entry(obj).State = EntityState.Modified;
            db.Add(obj);
        }

        public void Update(TEntity obj)
        {
            context.Entry(obj).State = EntityState.Modified;
        }

        public IEnumerable<TEntity> Find(Func<TEntity, Boolean> predicate)
        {
            return db.Where(predicate).ToList();
        }

        public void Delete(int id)
        {
            TEntity obj = db.Find(id);
            if (obj != null)
                db.Remove(obj);
        }

    }
}
