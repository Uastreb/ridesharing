﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RideSharingApp.DAL.Entities
{
    public class Companion
    {
        public int ClientId { get; set; }
        public Client Client { get; set; }

        public int PassingPointId { get; set; }
        public PassingPoint PassingPoint { get; set; }
    }
}
