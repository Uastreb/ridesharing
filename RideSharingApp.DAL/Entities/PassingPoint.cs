﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RideSharingApp.DAL.Entities
{
    public class PassingPoint
    {
        public int id { get; set; }
        public string OriginCoordinates { get; set; }
        public string EndCoordinates { get; set; }
        public DateTime DateAndTimeOfDepartue { get; set; }
        public DateTime DateAndTimeOfArrival { get; set; }
        public decimal Cost { get; set; }
        public int NumberPointOfThisTrip { get; set; }
        public bool Periodic { get; set; }


        public int TripId { get; set; }
        public Trip Trip { get; set; }
    }
}
