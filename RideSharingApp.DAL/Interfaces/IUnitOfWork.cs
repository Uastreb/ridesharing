﻿using System;
using RideSharingApp.DAL.Entities;

namespace RideSharingApp.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Client> Clients { get; }
        IRepository<Driver> Drivers { get; }
        IRepository<Car> Cars { get; }
        IRepository<Trip> Trips { get; }
        IRepository<PassingPoint> PassingPoints { get; }
        IRepository<Companion> Companions { get; }
        IRepository<AccountInformation> AccountsInformation { get; }
        IRepository<DriversLicense> DriversLicenses { get; }

        void Save();
    }
}
