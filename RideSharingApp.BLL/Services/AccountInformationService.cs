﻿using System;
using RideSharingApp.BLL.DTO;
using RideSharingApp.BLL.Infrastructure;
using RideSharingApp.BLL.Interfaces;
using RideSharingApp.DAL.Entities;
using RideSharingApp.DAL.Interfaces;
using System.Collections.Generic;
using AutoMapper;


namespace RideSharingApp.BLL.Services
{
    public class AccountInformationService : IAccountInformationService
    {
        IUnitOfWork Database { get; set; }

        public AccountInformationService(IUnitOfWork uow)
        {
            Database = uow;
        }

        public AccountInformationDTO GetAccountInformation(int? id)
        {
            if (id == null)
                throw new ValidationException("id информации об аккаунте не найдено", "");
            var accountInformation = Database.AccountsInformation.Get(id.Value);
            if (accountInformation == null)
                throw new ValidationException("Информация об аккаунте не найдена", "");

            return new AccountInformationDTO { id = accountInformation.id, Login = accountInformation.Login, Password = accountInformation.Password, Email = accountInformation.Email, DynamicSalt = accountInformation.DynamicSalt };
        }

        public IEnumerable<AccountInformationDTO> GetAccountsInformation()
        { 
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<AccountInformation, AccountInformationDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<AccountInformation>, List<AccountInformationDTO>>(Database.AccountsInformation.GetAll());
        }

        public void Registration(AccountInformationDTO account)
        {
            string staticSalt = "tko0yCAhVgc5V/ODk59hSQ==";
            (string password, string dynamicSalt) = MySecurityLib.Password.HashedPassword.GetHashedPassword(account.Password, staticSalt, "sha256");
            AccountInformation newAccount = new AccountInformation
            {
                Login = account.Login,
                Password = password,
                DynamicSalt = dynamicSalt,
                Email = account.Email
            };
            SendEmail.Send(account);
            Database.AccountsInformation.Create(newAccount);
            Database.Save();
        }



        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
