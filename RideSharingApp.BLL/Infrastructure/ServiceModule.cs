﻿using Ninject.Modules;
using RideSharingApp.DAL.Interfaces;
using RideSharingApp.DAL.Repositories;
using RideSharingApp.DAL.Entities;
using System.Data.Entity;

namespace RideSharingApp.BLL.Infrastructure
{
    public class ServiceModule : NinjectModule
    {
        private string connectionString;
        public ServiceModule(string connection)
        {
            connectionString = connection;
        }
        public override void Load()
        {
            Bind<IRepository<AccountInformation>>().To<TRepository<AccountInformation>>();   
        }
    }
}
