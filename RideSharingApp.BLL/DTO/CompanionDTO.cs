﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RideSharingApp.BLL.DTO
{
    public class Companion
    {
        public int ClientId { get; set; }
        
        public int PassingPointId { get; set; }
    }
}
