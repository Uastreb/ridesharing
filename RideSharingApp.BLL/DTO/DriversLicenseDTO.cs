﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RideSharingApp.BLL.DTO
{
    public class DriversLicenseDTO
    {
        public int id { get; set; }
        public DateTime DateOfReceiving { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string DriverLicensesNumber { get; set; }
        public string IssuingAuthority { get; set; }
        public Enum Categories { get; set; }

        public int DriverId { get; set; }
    }
}
