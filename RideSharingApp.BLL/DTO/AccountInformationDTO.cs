﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RideSharingApp.BLL.DTO
{
    public class AccountInformationDTO
    {
        public int id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string DynamicSalt { get; set; }
        public string Email { get; set; }
    }
}
