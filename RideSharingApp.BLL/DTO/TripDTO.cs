﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RideSharingApp.BLL.DTO
{ 
    public class TripDTO
    {
        public int id { get; set; }
        public int NumberOfSeats { get; set; }
        public Enum Status { get; set; }
        public DateTime RegistrationEndDate { get; set; }
        public bool Periodic { get; set; }


        public int CarId { get; set; }
    }
}
