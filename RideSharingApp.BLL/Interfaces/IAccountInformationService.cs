﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RideSharingApp.BLL.DTO;

namespace RideSharingApp.BLL.Interfaces
{
    public interface IAccountInformationService
    {
        IEnumerable<AccountInformationDTO> GetAccountsInformation();
        AccountInformationDTO GetAccountInformation(int? id);
        void Registration(AccountInformationDTO account);
        void Dispose();
    }
}
